# Pruebas js de criptografía y JWT

_Ejemplos de JavaScript con uso de criptografía y JWT._

## Comenzando 🚀

Estas instrucciones permitirán obtener una copia del proyecto en funcionamiento en cualquier máquina para propósitos de desarrollo y pruebas. La máquina virtual empleada para realizar esta práctica utiliza [Ubuntu 20.04 LTS](https://ubuntu.com/download/desktop), por lo que las instrucciones proporcionadas serán las adecuadas para un sistema que emplee **Ubuntu 20.04**.

Para obtener todos los archivos de la práctica, ejecuta la siguiente línea de código:
```
$ git clone https://asb127@bitbucket.org/asb127/auth-test.git
```

### Pre-requisitos 📋

Para la realización de esta práctica es necesario instalar [NodeJS](https://nodejs.org/en/). Primero, se instala el gestor de paquetes de Node (npm):

```
$ sudo apt update
$ sudo apt install npm
```

A continuación, se instala una utilidad para instalar y mantener las versiones de Node (llamada **n**):

```
$ sudo npm clean -f
$ sudo npm i -g n
```

Y se instala la última versión estable de Node:

```
$ sudo n stable
```

Finalmente, se deben instalar **bcrypt** (librería para poder encriptar contraseñas mediante hash) y **moment** (librería para poder encriptar contraseñas mediante hash):

```
$ npm i -S bcrypt
$ npm i -S moment
```


### Instalación 🔧

Creamos el archivo **01_bcrypt.js** en un editor de código para redactar un programa. 

```
$ cd ~/node/auth-test
$ code .
```

Este programa codificará una contraseña dos veces empleando dos entradas aleatorias distintas para la función hash. Uno de los resultados de esta función se comparará con la contraseña original y con otra distinta, lo cual debería obtener como resultado _true_ y _false_, respectivamente:

```js
'use strict'

// Formato del hash: 
//  $2b$10$.GFMCa5rH9wQiwRzDwb4S.QQDfXSU3l8HpCpDf1QHiQo5CkEgANxa
//  ****.. **********************+++++++++++++++++++++++++++++++
//  Alg Cost      Salt                     Hash
const bcrypt = require('bcrypt');

// Datos para simulacion
const miPass = 'miPass';
const badPass = 'mi OtraPass';

// Creamos el salt
bcrypt.genSalt(15, (err, salt) => {
    if (err) console.log(err);
    else console.log(`Salt 1: ${salt}`);

    // Utilizamos el Salt para genenrar un hash
    bcrypt.hash( miPass, salt, (err, hash) => {
        if (err) console.log(err);
        else console.log(`Hash 1: ${hash}`);
    });
});

// Creamos el hash directamente
bcrypt.hash( miPass, 10, (err, hash) => {
    if (err) console.log(err);
    else {
        console.log(`Hash 2: ${hash}`);

        // Comprobamos usando la contraseña correcta
        bcrypt.compare( miPass, hash, (err, result) => {
            if (err) console.log(err);
            else console.log(`Result 2.1: ${result}`);
        });
    
        // Comprobamos usando la contraseña incorrecta
        bcrypt.compare( miPass, badPass, (err, result) => {
            if (err) console.log(err);
            else console.log(`Result 2.2: ${result}`);
        });
    };
});
```

A continuación creamos una carpeta **services** en la que guardaremos los servicios que serán usados por otros programas. El primero será **pass.service.js**, el cual contendrá una función para encriptar contraseñas mediante hash y otra para comparar un valor hash con una contraseña:
```js
'use strict'

const bcrypt = require('bcrypt');

// encriptaPassword
//
// Devuelve un hash con salt en el formato
//  $2b$10$.GFMCa5rH9wQiwRzDwb4S.QQDfXSU3l8HpCpDf1QHiQo5CkEgANxa
//  ****.. **********************+++++++++++++++++++++++++++++++
//  Alg Cost      Salt                     Hash
function encriptaPassword( password ) {
    return bcrypt.hash ( password, 10 );
}

// comparaPassword
// Devolver true o false si coinciden o no el hash y el pass
function comparaPassword( password, hash ) {
    return bcrypt.compare ( password, hash );
}

module.exports = {
    encriptaPassword,
    comparaPassword
};
```

Este servicio será empleado por el programa **03_pass-test.js**, que usará las funciones de _pass-test.js_ para encriptar la contraseña de un usuario y realizar comparaciones entre esta y la contraseña encriptada, y entre una contraseña distinta y la contraseña encriptada:

```js
'use strict'

const PassService = require('./services/pass.service');
const moment = require('moment');

// Datos para simulacion
const miPass = 'miPass';
const badPass = 'mi OtraPass';
const usuario = {
    _id: '123456789',
    email: 'asb127@alu.ua.es',
    displayName: 'Antonio Sánchez',
    password: miPass,
    signUpDate: moment().unix(),
    lastLogin: moment().unix()
}

console.log(usuario);

// Encriptamos el password
PassService.encriptaPassword( usuario.password )
    .then( hash => {
        usuario.password = hash;
        console.log(usuario);

        // Verificamos el password
        PassService.comparaPassword( miPass, usuario.password)
        .then( isOk => {
            if( isOk ) {
                console.log('Prueba 1: El password es correcto');
            }
            else {
                console.log('Prueba 1: El password es incorrecto');
            }
        })
        .catch( err => console.log(err));

        // Verificamos el password contra un pass falso
        PassService.comparaPassword( badPass, usuario.password)
        .then( isOk => {
            if( isOk ) {
                console.log('Prueba 2: El password es correcto');
            }
            else {
                console.log('Prueba 2: El password es incorrecto');
            }
        })
        .catch( err => console.log(err));

    });
```

Creamos otro servicio que llamaremos **token.service.js**, el cual contendrá una función para crear un token **JWT** (JSON Web Token) a partir de un objeto JSON, y otra función para decodificar un token y comprobar su validez:

```js
'use strict'

const jwt = require('jwt-simple');
const moment = require('moment');

const SECRET = require('../config').secret;
const EXP_TIME = require('../config').tokenExpTmp;

// creaToken
//
// Devuelve un token JWT
// Formato:
//    HEADER.PAYLOAD.VERIFY_SIGNATURE
// Donde:
//    HEADER (Objeto JSON con el algoritmo HS256 de tipo JWT (JSON Web Tokens))
//        {
//          "alg": "HS256",
//          "typ": "JWT"
//        }
//    PAYLOAD (Datos del token)
//        {
//          sub: ID del usuario,
//          iat: Tiempo de creación del token
//          exp: Tiempo de expiración del token
//        }
//    VERIFY_SIGNATURE = HMACSHA256 ( base64UrlEncode(HEAD) + "." + base64UrlEncode(PAYLOAD), SECRET)
//
function creaToken( user ) {
    const payload = {
        sub: user._id,
        iat: moment().unix(),
        exp: moment().add(EXP_TIME, 'minutes').unix()
    };
    return jwt.encode( payload, SECRET );
}

// decodificaToken
//
// Devuelve el identificador del usuario
//
function decodificaToken( token ) {
    return new Promise( ( resolve, reject ) => {
        try {
            const payload = jwt.decode( token, SECRET, true );
            if( payload.exp <= moment().unix() ) {
                reject({
                    status: 401,
                    message: 'El token ha caducado'
                });
            }
            console.log( payload );
            resolve( payload.sub );
        } catch {
            reject( {
                status: 500,
                message: 'El token no es válido'
            });
        }
    });
}

module.exports = {
    creaToken,
    decodificaToken
};
```

Este servicio obtiene las variables _SECRET_ (cadena de caracteres secreta para crear la firma del JWT) y _EXP\_TIME_ (tiempo que será válido un JWT desde su creación, en segundos) de un archivo **config.js**, el cual define información a usar por los distintos servicios:

```js
module.exports = {
    db: 'localhost:27017/SD',
    port: 3100,
    secret: 'mipalabrasecreta',
    tokenExpTmp: 7*24*60
}
```

Las funciones de _token.service.js_ serán empleadas por el programa **04_token-test.js**, que creará un token JWT a partir de un objeto JSON _usuario_ y posteriormente lo decodificará para verificarlo. También se decodificará un token no válido, por lo que la función deberá devolver un error:

```js
'use strict'

const TokenService = require('./services/token.service');
const moment = require('moment');

// Datos para simulacion
const miPass = 'miPass';
const badPass = 'mi OtraPass';
const usuario = {
    _id: '123456789',
    email: 'asb127@alu.ua.es',
    displayName: 'Antonio Sánchez',
    password: miPass,
    signUpDate: moment().unix(),
    lastLogin: moment().unix()
};

console.log(usuario);

// Creamos un token
const token = TokenService.creaToken( usuario );
// console.log(token);

// Decodificamos un token
TokenService.decodificaToken( token )
    .then( userId => {
        return console.log(`ID1: ${userId}`);
    })
    .catch ( err => console.log( err ));

// Decodificamos un token erróneo
const badToken = 'EyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIwMTIzNDU2Nzg5MCIsIm5hbWUiOiJKb2huIERvZSIsImlhdCI6MTUxNjIzOTAyMn0.vizHrSWFJ48SOiDYonkkG8eUKPw_y28NM6e0STxnZeM';
TokenService.decodificaToken( badToken )
    .then( userId => {
        return console.log(`ID2: ${userId}`);
    })
    .catch ( err => console.log( err ));
```

## Ejecutando las pruebas ⚙️

### Pruebas
Probamos mediante la terminal que los distintos programas de JavaScript funcionan correctamente:

**Prueba 01_bcrypt**

```
$ node 01_bcrypt.js
```

Obtenemos la siguiente respuesta:

```
Salt 1: $2b$15$zUAf/NFJ1zWnDCGBY2X3xO
Hash 2: $2b$10$PqOLWe5YL.wGSjl6ySQ3F.cALtxEnpADonR4LlXvjxKyMLMh9tPyy
Result 2.2: false
Result 2.1: true
Hash 1: $2b$15$zUAf/NFJ1zWnDCGBY2X3xOJ6/Wd7K7kT3IiiC6lH5CvbDHHYT3BOK
```

Cabe destacar que, al usar un valor mayor como factor en la creación de _Hash 1_ respecto a _Hash 2_, el proceso es más lento y finaliza unos segundos más tarde.

**Prueba 03_pass-test**

```
$ node 03_pass-test.js
```

Obtenemos la siguiente respuesta:

```
{
  _id: '123456789',
  email: 'asb127@alu.ua.es',
  displayName: 'Antonio Sánchez',
  password: 'miPass',
  signUpDate: 1647604394,
  lastLogin: 1647604394
}
{
  _id: '123456789',
  email: 'asb127@alu.ua.es',
  displayName: 'Antonio Sánchez',
  password: '$2b$10$GsrAdPioUtBNryQ7x6NasOoBYhYkvKdVoxrdUpmCYBJb1e8/mtfoK',
  signUpDate: 1647604394,
  lastLogin: 1647604394
}
Prueba 2: El password es incorrecto
Prueba 1: El password es correcto
```

**Prueba 04_token-test**

```
$ node 04_token-test.js
```

Obtenemos la siguiente respuesta:

```
{
  _id: '123456789',
  email: 'asb127@alu.ua.es',
  displayName: 'Antonio Sánchez',
  password: 'miPass',
  signUpDate: 1647604434,
  lastLogin: 1647604434
}
{ sub: '123456789', iat: 1647604434, exp: 1648209234 }
ID1: 123456789
{ status: 500, message: 'El token no es válido' }
```

## Construido con 🛠️

* [VS Code](https://code.visualstudio.com/) - Editor de código usado para programar 

## Versionado 📌

Para todas las versiones disponibles, mira los [commits en este repositorio](https://bitbucket.org/asb127/auth-test/commits/).

## Autores ✒️

* **Paco Maciá** - *Guía de la práctica* - [pmacia](https://github.com/pmacia)
* **Antonio Sánchez** - *Trabajo y documentación* - [asb127](https://bitbucket.org/asb127/)
