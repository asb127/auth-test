'use strict'

const bcrypt = require('bcrypt');

// encriptaPassword
//
// Devuelve un hash con salt en el formato
//  $2b$10$.GFMCa5rH9wQiwRzDwb4S.QQDfXSU3l8HpCpDf1QHiQo5CkEgANxa
//  ****.. **********************+++++++++++++++++++++++++++++++
//  Alg Cost      Salt                     Hash
function encriptaPassword( password ) {
    return bcrypt.hash ( password, 10 );
}

// comparaPassword
// Devolver true o false si coinciden o no el hash y el pass
function comparaPassword( password, hash ) {
    return bcrypt.compare ( password, hash );
}

module.exports = {
    encriptaPassword,
    comparaPassword
};
