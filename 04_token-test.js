'use strict'

const TokenService = require('./services/token.service');
const moment = require('moment');

// Datos para simulacion
const miPass = 'miPass';
const badPass = 'mi OtraPass';
const usuario = {
    _id: '123456789',
    email: 'asb127@alu.ua.es',
    displayName: 'Antonio Sánchez',
    password: miPass,
    signUpDate: moment().unix(),
    lastLogin: moment().unix()
};

console.log(usuario);

// Creamos un token
const token = TokenService.creaToken( usuario );
// console.log(token);

// Decodificamos un token
TokenService.decodificaToken( token )
    .then( userId => {
        return console.log(`ID1: ${userId}`);
    })
    .catch ( err => console.log( err ));

// Decodificamos un token erróneo
const badToken = 'EyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIwMTIzNDU2Nzg5MCIsIm5hbWUiOiJKb2huIERvZSIsImlhdCI6MTUxNjIzOTAyMn0.vizHrSWFJ48SOiDYonkkG8eUKPw_y28NM6e0STxnZeM';
TokenService.decodificaToken( badToken )
    .then( userId => {
        return console.log(`ID2: ${userId}`);
    })
    .catch ( err => console.log( err ));
