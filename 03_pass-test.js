'use strict'

const PassService = require('./services/pass.service');
const moment = require('moment');

// Datos para simulacion
const miPass = 'miPass';
const badPass = 'mi OtraPass';
const usuario = {
    _id: '123456789',
    email: 'asb127@alu.ua.es',
    displayName: 'Antonio Sánchez',
    password: miPass,
    signUpDate: moment().unix(),
    lastLogin: moment().unix()
};

console.log(usuario);

// Encriptamos el password
PassService.encriptaPassword( usuario.password )
    .then( hash => {
        usuario.password = hash;
        console.log(usuario);

        // Verificamos el password
        PassService.comparaPassword( miPass, usuario.password)
        .then( isOk => {
            if( isOk ) {
                console.log('Prueba 1: El password es correcto');
            }
            else {
                console.log('Prueba 1: El password es incorrecto');
            }
        })
        .catch( err => console.log(err));

        // Verificamos el password contra un pass falso
        PassService.comparaPassword( badPass, usuario.password)
        .then( isOk => {
            if( isOk ) {
                console.log('Prueba 2: El password es correcto');
            }
            else {
                console.log('Prueba 2: El password es incorrecto');
            }
        })
        .catch( err => console.log(err));

    });
