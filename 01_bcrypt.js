'use strict'

// Formato del hash: 
//  $2b$10$.GFMCa5rH9wQiwRzDwb4S.QQDfXSU3l8HpCpDf1QHiQo5CkEgANxa
//  ****.. **********************+++++++++++++++++++++++++++++++
//  Alg Cost      Salt                     Hash
const bcrypt = require('bcrypt');

// Datos para simulacion
const miPass = 'miPass';
const badPass = 'mi OtraPass';

// Creamos el salt
bcrypt.genSalt(15, (err, salt) => {
    if (err) console.log(err);
    else console.log(`Salt 1: ${salt}`);

    // Utilizamos el Salt para genenrar un hash
    bcrypt.hash( miPass, salt, (err, hash) => {
        if (err) console.log(err);
        else console.log(`Hash 1: ${hash}`);
    });
});

// Creamos el hash directamente
bcrypt.hash( miPass, 10, (err, hash) => {
    if (err) console.log(err);
    else {
        console.log(`Hash 2: ${hash}`);

        // Comprobamos usando la contraseña correcta
        bcrypt.compare( miPass, hash, (err, result) => {
            if (err) console.log(err);
            else console.log(`Result 2.1: ${result}`);
        });
    
        // Comprobamos usando la contraseña incorrecta
        bcrypt.compare( miPass, badPass, (err, result) => {
            if (err) console.log(err);
            else console.log(`Result 2.2: ${result}`);
        });
    };
});
